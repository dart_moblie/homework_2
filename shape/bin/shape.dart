import 'package:shape/shape.dart' as shape;
import 'dart:math';

abstract class Shape {
  double get area;
}

class Square extends Shape {
  Square(this.side);
  final double side;

  @override
  double get area => side * side;
}

class Circle extends Shape {
  Circle(this.radius);
  final double radius;

  @override
  double get area => pi * radius * radius;
}

class Rectangle extends Shape {
  Rectangle(this.width, this.height);
  final double width, height;

  @override
  double get area => width * height;
}

void main(List<String> args) {
  final s = Square(10.0);
  print("Square Area :");
  print(s.area);

  final c = Circle(4.0);
  print("Circle Area :");
  print(c.area);

  final r = Rectangle(5.0, 8.0);
  print("Rectangle Area :");
  print(r.area);
}
